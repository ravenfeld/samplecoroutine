package com.ravenfeld.sample.coroutine


interface FakeView {

    fun showError(message: String)

    fun showErrorInternet()

    fun showLoading()

    fun hideLoading()

    fun renderMovie(movieViewModel: MovieViewModel )
}