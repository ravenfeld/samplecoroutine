package com.ravenfeld.sample.coroutine

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.ravenfeld.sample.coroutine.domain.interactor.GetMovie
import kotlinx.android.synthetic.main.activity_main.*


class HomeActivity : AppCompatActivity(), FakeView {

    private val moviePresenter: FakePresenter by lazy {
        val getMovie = GetMovie((application as TestApplication).movieRepository)
        FakePresenter(this, this, getMovie)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        moviePresenter.initialize(this)
    }

    override fun showError(message: String) {
        Snackbar.make(findViewById<View>(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
    }

    override fun showErrorInternet() {
        Snackbar.make(findViewById<View>(android.R.id.content), getString(R.string.no_network_connection), Snackbar.LENGTH_LONG).show()
    }

    override fun showLoading() {
        progressCircular.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressCircular.visibility = View.GONE
    }

    override fun renderMovie(movieViewModel: MovieViewModel) {
        text.text = movieViewModel.title
    }
}