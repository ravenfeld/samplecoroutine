package com.ravenfeld.sample.coroutine

import android.app.Application
import com.ravenfeld.sample.coroutine.data.repository.FakeDataRepository
import com.ravenfeld.sample.coroutine.domain.repository.MovieRepository


class TestApplication : Application() {

    val movieRepository: MovieRepository by lazy {
        FakeDataRepository(applicationContext)
    }
}