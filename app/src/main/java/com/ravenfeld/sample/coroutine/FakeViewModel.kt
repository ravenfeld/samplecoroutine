package com.ravenfeld.sample.coroutine

import com.ravenfeld.sample.coroutine.domain.Movie


data class MovieViewModel(val title: String)

internal fun movieViewModelMapper(movie: Movie) = MovieViewModel(movie.title)