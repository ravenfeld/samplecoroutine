package com.ravenfeld.sample.coroutine

import androidx.lifecycle.*
import com.ravenfeld.sample.coroutine.domain.Movie
import com.ravenfeld.sample.coroutine.domain.interactor.GetMovie
import java.net.UnknownHostException


class FakePresenter(private val lifecycleOwner: LifecycleOwner, private val viewModelStoreOwner: ViewModelStoreOwner, private val getMovie: GetMovie) {

    var view: FakeView? = null

    fun initialize(view: FakeView) {
        this.view = view
        showViewLoading()
        getMovie()
    }

    private fun showViewLoading() {
        view?.showLoading()
    }

    private fun hideViewLoading() {
        view?.hideLoading()
    }

    private fun getMovie() {
        val model = ViewModelProvider(viewModelStoreOwner, FakeViewModelLiveDataFactory(getMovie))
                .get(FakeViewModelLiveData::class.java)
        model.movie.observe(lifecycleOwner,  Observer<Result<Movie>> { result ->
            hideViewLoading()
            try {
                view?.renderMovie(movieViewModelMapper(result.getOrThrow()))
            } catch (e: Exception) {
                if (e is UnknownHostException) {
                    view?.showErrorInternet()
                } else {
                    view?.showError("onError : " + e.message)
                }
            }
        })
    }
}