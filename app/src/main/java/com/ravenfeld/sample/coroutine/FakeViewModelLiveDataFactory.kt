package com.ravenfeld.sample.coroutine

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ravenfeld.sample.coroutine.domain.interactor.GetMovie


class FakeViewModelLiveDataFactory(private val getMovie: GetMovie) :
        ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            if (modelClass.isAssignableFrom(FakeViewModelLiveData::class.java)) {
                FakeViewModelLiveData(getMovie) as T
            } else throw IllegalArgumentException()
}