package com.ravenfeld.sample.coroutine

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.ravenfeld.sample.coroutine.domain.interactor.GetMovie
import kotlinx.coroutines.Dispatchers


class FakeViewModelLiveData(private val getMovie: GetMovie) : ViewModel() {

    val movie = liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
        try {
            val data = getMovie.buildUseCase()
            emit(Result.success(data))
        } catch (e: Exception) {
            emit(Result.failure(e))
        }
    }

}