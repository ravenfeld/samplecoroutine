package com.ravenfeld.sample.coroutine.domain.interactor

import com.ravenfeld.sample.coroutine.domain.Movie
import com.ravenfeld.sample.coroutine.domain.repository.MovieRepository


class GetMovie(private val movieRepository: MovieRepository) {

    suspend fun buildUseCase(): Movie = movieRepository.getMovie()

}