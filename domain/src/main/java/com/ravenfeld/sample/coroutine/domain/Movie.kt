package com.ravenfeld.sample.coroutine.domain


data class Movie(val id: Int, val title: String)