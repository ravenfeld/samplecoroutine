package com.ravenfeld.sample.coroutine.domain.repository

import com.ravenfeld.sample.coroutine.domain.Movie


interface MovieRepository {
    suspend fun getMovie(): Movie
}