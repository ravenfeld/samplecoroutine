package com.ravenfeld.sample.coroutine.data.entity

import com.google.gson.annotations.SerializedName

data class FakeEntity(
        @SerializedName("id")
        var id: Int,
        @SerializedName("title")
        var title: String
)
