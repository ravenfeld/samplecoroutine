package com.ravenfeld.sample.coroutine.data.ws

import android.content.Context
import android.util.Log
import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.logger.Loggable
import com.ravenfeld.sample.coroutine.data.entity.FakeEntity
import com.ravenfeld.sample.coroutine.data.URL
import com.ravenfeld.sample.coroutine.data.BuildConfig
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ClientFake(private val context: Context) {

    private val client: ServiceFake by lazy {
        val builderHttp = OkHttpClient.Builder()

        builderHttp.addInterceptor(ChuckInterceptor(context))

        if (BuildConfig.DEBUG) {
            builderHttp.addInterceptor(CurlInterceptor(Loggable { message -> Log.v("Ok2Curl", message) }))
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builderHttp.addInterceptor(httpLoggingInterceptor)

        }

        Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builderHttp.build())
                .build().create<ServiceFake>(ServiceFake::class.java)
    }

    suspend fun getMovie(): FakeEntity {
        return client.getMovie()
    }
}