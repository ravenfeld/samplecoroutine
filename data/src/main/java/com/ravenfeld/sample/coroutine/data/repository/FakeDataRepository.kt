package com.ravenfeld.sample.coroutine.data.repository

import android.content.Context
import com.ravenfeld.sample.coroutine.data.mapper.movieMapper
import com.ravenfeld.sample.coroutine.data.ws.ClientFake
import com.ravenfeld.sample.coroutine.domain.Movie
import com.ravenfeld.sample.coroutine.domain.repository.MovieRepository


class FakeDataRepository(context: Context) : MovieRepository {
    var client: ClientFake = ClientFake(context)

    override suspend fun getMovie(): Movie {
        val movieEntity = client.getMovie()
        return movieMapper(movieEntity)
    }
}