package com.ravenfeld.sample.coroutine.data.mapper

import com.ravenfeld.sample.coroutine.data.entity.FakeEntity
import com.ravenfeld.sample.coroutine.domain.Movie


internal fun movieMapper(movieEntity: FakeEntity): Movie {
    return Movie(movieEntity.id, movieEntity.title)
}