package com.ravenfeld.sample.coroutine.data.ws

import com.ravenfeld.sample.coroutine.data.entity.FakeEntity
import retrofit2.http.GET


interface ServiceFake {
    @GET("todos/1")
    suspend fun getMovie(): FakeEntity
}